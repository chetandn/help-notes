REM *************************************************************
REM To view serial COM port through command line
REM *************************************************************
chgport 
mode /? 
REM !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

REM *************************************************************
REM To print config spec used in clear case
REM *************************************************************
echo ConfigSpec Used :
cleartool catcs
REM !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

REM *************************************************************
REM To redirect STD error to STDout
REM *************************************************************
some_command >%temp%\temp.txt 2>&1
REM !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

REM *************************************************************
REM To check windows is 32 bit or 64 bit and jump accordingly
REM *************************************************************
IF EXIST "%PROGRAMFILES(X86)%" (GOTO 64BIT) ELSE (GOTO 32BIT)
REM !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

REM *************************************************************
REM Batch File Scripts
REM *************************************************************
ECHO OFF
SET CURRENT_SCRIPT_DIR_PATH=%~dp0
ECHO %CURRENT_SCRIPT_DIR_PATH%

SET CURRENT_SCRIPT_DIR_PATH=%CURRENT_SCRIPT_DIR_PATH%\append_var\
ECHO %CURRENT_SCRIPT_DIR_PATH%

SET CURRENT_DRIVE_LETTER=%~d0
ECHO %CURRENT_DRIVE_LETTER%
REM above prints DriveLetter+: i.e C:, D: , Z:

REM Truncate string %STRING:~StartIndex,LengthOfSubString-From-StartIndex% , { if LengthOfSubString mentioned > length of string ? length of string : LengthOfSubString mentioned }
SET GET_DRIVE_LETTER_ONLY=%CURRENT_DRIVE_LETTER:~0,1%
ECHO %GET_DRIVE_LETTER_ONLY%

REM Print new line when echo is off
ECHO. 
REM !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

REM *************************************************************
REM If folder not exists create, if present do nothing
REM *************************************************************
SET TEMP_DIR=C:\TEMP99
IF NOT EXIST %TEMP_DIR% MKDIR "%TEMP_DIR%"
REM !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

REM *************************************************************
REM Displays all commands history stored in memory.
REM *************************************************************
doskey /HISTORY
REM !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

REM *************************************************************
REM Regex alternative
REM *************************************************************
REM IF found regex  , batch alternative - &&
echo %PERL_FOUND_FLAG% | findstr /i /r /C:"strawberry.*" && ( echo found )
REM IF Not found regex , batch alterantive - ||
echo %PERL_FOUND_FLAG% | findstr /i /r /C:"strawberryXYZ.*" || ( echo Not found)
REM !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

REM *************************************************************
REM -----------------Setting Strawberry Perl Path if not exists earlier-----------------------------
REM *************************************************************

for %%X in (perl.exe) do (set PERL_FOUND_FLAG=%%~$PATH:X) 
(
	IF NOT DEFINED PERL_FOUND_FLAG ( 
		ECHO %PERL_FOUND_FLAG% perl not found
		ECHO SET PATH=C:\Tools\Strawberry\Perl64\c\bin;C:\Tools\Strawberry\Perl64\perl\bin;%PATH%
	) ELSE ( 
		ECHO %PERL_FOUND_FLAG% found 
		ECHO Checking Strawberry perl
		
		REM Check whether its Strawberry perl or not
		echo %PERL_FOUND_FLAG% | findstr /i /r /C:"strawberry.*" && ( echo strawberry perl found. no changes needed)
		SET PERL_FOUND_FLAG2="C:\Tools\Active\Perl64\perl\bin\perl.exe"
		echo %PERL_FOUND_FLAG2% | findstr /i /r /C:"strawberry.*" || (
			echo Not found
			echo setting path
			SET PATH=C:\Tools\Strawberry\Perl64\c\bin;C:\Tools\Strawberry\Perl64\perl\bin;%PATH%
		)

	)
)
REM !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

REM *************************************************************
REM -----------------Setting XTCHOME User Environment Variable-----------------------------
REM *************************************************************
IF NOT DEFINED XTCHOME ( 
ECHO Setting XTCHOME 
SETX XTCHOME C:\Tools\XTC_V7_1
) ELSE ( 
ECHO Already defined XTCHOME
)
REM !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

REM *************************************************************
REM -- Zip all folders in directory ---
REM *************************************************************
for /d %%X in (*) do "C:\Tools\7-Zip_15_14_64bit\7z.exe" a "%%X.7z" "%%X\."
REM !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


REM *************************************************************
REM SET Metered Network Command Line
REM *************************************************************

netsh wlan show profiles

netsh wlan show profile name="Redmi Y1"

netsh wlan set profileparameter name="Redmi Y1" cost=Variable
REM !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


REM *************************************************************
REM 
REM *************************************************************

REM !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!