@ECHO OFF

SET CURR_PATH_DIR=%~dp0

SET JENKINS_JOBS_PATH=C:\Jenkins\jobs

CD %JENKINS_JOBS_PATH%

rmdir /s /q C:\Jenkins\jobs\COMPILATION\builds\
rmdir /s /q C:\Jenkins\jobs\CTC_REPORT_CONSOLIDATOR\builds\
rmdir /s /q C:\Jenkins\jobs\DASHBOARD_UPDATER\builds\
rmdir /s /q C:\Jenkins\jobs\EXECUTION\builds\
rmdir /s /q C:\Jenkins\jobs\JENKINS_LOGCOPY\builds\


@(ECHO 1) >C:\Jenkins\jobs\COMPILATION\nextBuildNumber
@(ECHO 1) >C:\Jenkins\jobs\CTC_REPORT_CONSOLIDATOR\nextBuildNumber
@(ECHO 1) >C:\Jenkins\jobs\DASHBOARD_UPDATER\nextBuildNumber
@(ECHO 1) >C:\Jenkins\jobs\EXECUTION\nextBuildNumber
@(ECHO 1) >C:\Jenkins\jobs\JENKINS_LOGCOPY\nextBuildNumber

:PROCEED
CD /D %CURR_PATH_DIR%
pause